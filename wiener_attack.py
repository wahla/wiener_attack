#!/usr/bin/env python3

import argparse
from Crypto.PublicKey import RSA
import owiener



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i",help="input rsa public key file",dest="input",required=True)
    parser.add_argument("-o",help="output rsa private key file",dest="out",required=True)
    args=parser.parse_args()

    input_file=args.input
    output_file=args.out

    public_key = RSA.importKey(open(input_file, 'r').read())

    print("n="+str(public_key.n))
    print("e="+str(public_key.e))

    d = owiener.attack(public_key.e, public_key.n)

    if d is None:
        print("Attack has failed :-(")
        return(1)
    else:
        print("Attack was successfull!  d={}".format(d))

    private_key = RSA.construct((public_key.n, public_key.e, d))
    print(private_key.exportKey())
    with open(output_file,"wb") as f:
        f.write(private_key.exportKey())

if __name__=="__main__":
    main()

